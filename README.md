ML-PPA (Machine Learning-based Pipeline for Pulsar Analysis) is a framework for extracting pulsar signals in data streams from radio astronomical antennas. It models various classes of Radio Frequency Interference (RFI) signals, i.e., background noise emanating from devices such as cell phones or satellites. ML-PPA can be used to generate synthetic data by simulating the path of pulsar signals from source to measurement by telescopes. 2D images (time-frequency) with pulsar and RFI signals can be generated directly. Neural network models for classifying real and synthetic data are provided. 


Running ML-PPA
--------------
The current version is ML-PPA 0.2. The repositories of the previous version 0.1 can be viewed in the individual repositories by selecting *Releases*. 

Four pipelines are provided:
- PulsarDT: simulation of the propagation of pulsar signals from the source 
  to antennas and generation of synthetic data (digital twins) - written in Python,
- PulsarDT++: a C++ version of PulsarDT, 
- PulsarRFI_Gen: generation of 2D images (time-frequency) with various classes of 
  pulsar and RFI signals, 
- PulsarRFI_NN: neural networks for identification of pulsar and RFI signals 
  in 2D images (time-frequency).


To get started move to the project "tutorial_projects".  



Documentation for ML-PPA 
-----------------------------
For a datailed description of the framework, see the [document](PUNCH_interTwin_project.pdf).

Further details about the framework are available in the source code of its objects.